#include <iostream>

using namespace std;

int main()
{
    char c;
    cout <<"\n ( This program is code by Bilal Hassan Stanikzai )"<<endl;
    cout <<"\n This program show the ASCI code of a characters :"<<endl;
    cout <<"\n Please enter a character :"<<endl;
    cin >>c;
    cout <<"\n You enter :"<<"\t"<<c<<endl;
    cout <<"\n ASCI vale of "<<c<<" is :  "<<int(c)<<endl;
    return 0;
}
